// Gmsh project created on Thu Apr  4 08:53:35 2019
SetFactory("OpenCASCADE");
//+
Circle(1) = {0, 0, 0, 9, 0, Pi};//+
Line(2) = {1, 2};
//+
Curve Loop(1) = {1, -2};
//+
Plane Surface(1) = {1};
//+
Disk(2) = {0, 1, 0, 0.25, 0.25};
//+//+
BooleanDifference{ Surface{1}; Delete; }{ Surface{2}; Delete; }
//+
Physical Curve("outer_conductor", 1) = {4};
//+
Physical Curve("inner_conductor", 2) = {3};
//+
Physical Curve("gnd_plane", 3) = {5};
//+
Physical Surface("background", 4) = {1};
//+
Transfinite Curve {4} = 120 Using Progression 1;
//+
Transfinite Curve {3} = 30 Using Progression 1;
//+
Transfinite Curve {5} = 60 Using Progression 1;
