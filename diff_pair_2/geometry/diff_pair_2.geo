// Gmsh project created on Mon Jul 15 01:07:31 2019
SetFactory("OpenCASCADE");
//+
Circle(1) = {0, 0, 0, 9, 0, Pi/2};
//+
Circle(2) = {0, 1, 0, 0.25, -Pi/2, Pi/2};
//+
Point(5) = {0, 0, 0, 1.0};
//+
Line(3) = {1, 3};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 2};
//+
Curve Loop(1) = {3, 2, 4, 5, -1};
//+
Plane Surface(1) = {1};
//+
Physical Curve("inner", 1) = {2};
//+
Physical Curve("outer", 2) = {1};
//+
Physical Curve("neumann", 3) = {3, 4};
//+
Physical Curve("dirichlet", 4) = {5};
//+
Physical Surface("background", 5) = {1};
//+
Transfinite Curve {3} = 40 Using Progression 1;
//+
Transfinite Curve {5} = 60 Using Progression 1;
//+
Transfinite Curve {4} = 10 Using Progression 1;
//+
Transfinite Curve {1} = 100 Using Progression 1;
//+
Transfinite Curve {2} = 20 Using Progression 1;
