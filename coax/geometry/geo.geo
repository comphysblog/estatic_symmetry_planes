// Gmsh project created on Thu Mar 28 09:27:08 2019
SetFactory("OpenCASCADE");
//+
Circle(1) = {0, 0, 0, 9, 0, Pi/2};
//+
Circle(2) = {0.0, 0.0, 0, 1.5, 0, Pi/2};
//+
Line(3) = {1, 3};
//+
Line(4) = {4, 2};
//+
Line Loop(1) = {1, -4, -2, -3};
//+
Line Loop(2) = {3, 2, 4, -1};
//+
Plane Surface(1) = {2};
//+
Physical Surface("dielectric", 1) = {1};
//+
Physical Line("inner_conductor", 2) = {2};
//+
Physical Line("outer_conductor", 3) = {1};
//+
Transfinite Line {2} = 20 Using Progression 1;
//+
Transfinite Line {1} = 50 Using Progression 1;
//+
Transfinite Line {3, 4} = 50 Using Progression 1;
